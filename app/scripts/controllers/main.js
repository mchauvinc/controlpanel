'use strict';

controlpanelApp.controller('MainCtrl', function ($scope, genresService, $http, $rootScope) {
    $scope.genres = genresService.query();
    $scope.doSave = function () {
        genresService.save({ data: angular.toJson( $scope.genres ) });
    }
});
