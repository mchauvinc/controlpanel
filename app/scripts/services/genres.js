'use strict';

controlpanelApp.value('genres', [
    {
        genre_id: 1, 
        title: "Movies", 
        genre_key: 20, 
        subgenres: [
            {
                subgenre_id: 1,
                title: "Scary",
                subgenre_key: 42
            },
            {
                subgenre_id: 3,
                title: "Action",
                subgenre_key: 9
            }
        ]
    },
    {
        genre_id: 2,
        title: "Education",
        genre_key: 16,
        subgenres: [
        ]
    }
]
);
